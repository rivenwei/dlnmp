# dlnmp

#### 介绍
一个镜像包含lnmp

#### 软件架构
软件架构说明


#### 安装教程
```yml
# 项目共享
/home/wwwroot
# nginx共享
/home/wwwroot
/usr/local/nginx/conf
/home/wwwlogs

-${SOURCE_DIR}:/var/www/html/:rw
-${NGINX_CONFD_DIR}:/etc/nginx/conf.d/:rw
-${NGINX_CONF_FILE}:/etc/nginx/nginx.conf:ro
-${NGINX_LOG_DIR}:/var/log/nginx/:rw


# 容器命令
docker run
--privileged
-it
--name my
-p 3306:3306 -p 6379:6379 -p 80:80 -p 9515:9515 -p 22:22
-v F:\project\my_centos7\wwwroot:/home/wwwroot
-v F:\project\my_centos7\conf\nginx\conf:/usr/local/nginx/conf
-v F:\project\my_centos7\log:/home/wwwlogs
-v F:\project\my_centos7\conf\php:/usr/local/php/etc
-v F:\project\my_centos7\src:/usr/local/src
-v F:\project\my_centos7\conf\redis:/etc/redis
registry.cn-hangzhou.aliyuncs.com/riven/centos_lnmp

# window运行容器命令
docker run --privileged -it --name my 
-p 3306:3306 
-p 6379:6379 
-p 80:80 
-p 9515:9515 
-p 22:22 
-v F:\project\my_centos7\wwwroot:/home/wwwroot 
-v F:\project\my_centos7\conf\nginx\conf:/usr/local/nginx/conf 
-v F:\project\my_centos7\log:/home/wwwlogs 
-v F:\project\my_centos7\conf\php:/usr/local/php/etc 
-v F:\project\my_centos7\src:/usr/local/src 
-v F:\project\my_centos7\conf\redis:/etc/redis registry.cn-hangzhou.aliyuncs.com/riven/centos_lnmp

# 容器配置信息&常用命令
php -m | grep -i redis
cp -f /usr/local/src/phpredis-5.0.0/modules/redis.so /usr/lib64/php/modules/redis.so
docker cp my:/usr/local/php/etc/ F:\project\my_centos7\conf\php
```

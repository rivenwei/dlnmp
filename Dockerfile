# 定义在构建镜像时所用的变量,在将来容器运行时是不会存在这些环境变量的
ARG IMAGE="registry.cn-hangzhou.aliyuncs.com/riven/centos_lnmp:1.2"
# 基础镜像
FROM ${IMAGE}
# 指定运行容器时的用户名或UID,后续的RUN也会使用指定用户
USER root:root
# 镜像维护者
MAINTAINER riven<1054487195@qq.com>
# 设置环境变量
ENV mypath /root
# 默认工作目录
WORKDIR ${mypath}
# 暴露端口
EXPOSE 22
EXPOSE 9515
# 容器启动时要运行的命令, 不会被docker run之后的参数替换
ENTRYPOINT /bin/bash
#设置子镜像的触发操作
ONBUILD RUN echo "on build excuted"